# instant-project


## Requirements

- Docker
- Docker-compose
- GNU Make
- Free port 80 (nginx)
- have [hostsupdater](https://github.com/nstapelbroek/hostupdater) running connected to a docker network called `hostsupdater`
- a Kubernetes cluster to deploy to

## Usage

- clone this project
- choose a name for your intended project, ie: foo-bar
- run `mv instant-project foo-bar && cd foo-bar && ./init.sh`
- run `make init`
